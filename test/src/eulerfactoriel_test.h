// Classe qui teste la classe Euler
// Avec le framework CppUnit

// Librairies CppUnit nécessaires.
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

// Autres librairies
#include <exception>

// Le fichier à tester, qui se trouve dans un répertoire différent.
#include "../../src/euler.h"

class EulerFactorielTest : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(EulerFactorielTest);
    CPPUNIT_TEST(test_estimerValeur_un);
    CPPUNIT_TEST(test_estimerValeur_exception);
    CPPUNIT_TEST_SUITE_END();
    
private:
	Euler* objet_a_tester;
    
public:
	// Fonctions d'échafaudage
    void setUp();
    void tearDown();
    
    // Fonctions de tests
    void test_estimerValeur_un();
    void test_estimerValeur_exception();
};
