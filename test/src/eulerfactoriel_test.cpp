
#include "eulerfactoriel_test.h"

void EulerFactorielTest::setUp() {
		this->objet_a_tester = new Euler();
}

void EulerFactorielTest::tearDown() {
		delete this->objet_a_tester;
}

// Correspond à d1 = <{1}, {2}>
void EulerFactorielTest::test_estimerValeur_un() {
	float obtenue = this->objet_a_tester->estimerValeur(1);
    float attendue = 2.0;

    // Problème de comparaison de variable flottante. Pour l'explication, voir :
	// voir https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
    bool resultat = (obtenue - attendue) < FLT_EPSILON;
    CPPUNIT_ASSERT(resultat);
}

// Correspond à d1 = <{-5}, {std::exception}>
void EulerFactorielTest::test_estimerValeur_exception() {
	CPPUNIT_ASSERT_THROW(this->objet_a_tester->estimerValeur(-5), std::exception);
}
