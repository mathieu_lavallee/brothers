
#include "euler.h"

Euler::Euler() {
	this->f = new Factoriel();
}

Euler::~Euler() {
	delete this->f;
}

float Euler::estimerValeur(int iterations) {
	
    if (iterations < 0) throw std::exception();

	float estimation = 0;
	
	// Méthode de Brothers
	// Sommation(i=0..n) de {(2i+2)/(2i+1)!}
	for(int i=0 ; i<iterations ; i++) {
		estimation = estimation + float(2*i+2) / this->f->calculerFactoriel(2*i+1);
	}
	
    return estimation;
}

